//============================================================================
// Name        : UDPLibDemo.cpp
// Author      : yu_lei
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "..\UDPLib\SimpleUDPLib.h"

#ifdef _WIN32 || _WIN64

#include<windows.h>

#else

#endif

using namespace std;

int main() {
	char recv_buf[1024];
	int tick_counter = 0;
	udplib_initialize();
	udplib_send("192.168.199.190:8888","192.168.199.199:9999","send data\n",10);
	udplib_setblockmode("192.168.199.199:8888",0);
	// udplib_recv();
	while(1)
	{
		int recv_len;
		udplib_send("192.168.199.190:8888", "192.168.199.199:9999", recv_buf,10);
		recv_len = udplib_recv("0.0.0.0:9999", "192.168.199.199:8888",recv_buf,1024);
		if(recv_len > 0)
		{
			recv_buf[recv_len] = '\0';
			printf("recv data : %s\n", recv_buf);
		}
		printf("tick counts %d\n",tick_counter++);
#ifdef _WIN32 || _WIN64
		Sleep(1);
#else
		sleep(1);
#endif
	}
	return 0;
}

